# 2D_Platformer_with_Java_Swing

This repository was created for practicing design patterns and simple game engine work systems. The game created using Java Swing UI system.

![2DPlatformer](2DPlatformer.gif)

The game has the following features;
 - Strategy Design Pattern for High and Low Jump
 - Decorator Design Pattern in Point System
 - Game Over and Start Screens
