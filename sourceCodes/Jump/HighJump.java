package Jump;
import GameObjects.Player;

public class HighJump implements JumpBehavior {

	@Override
	public void jump(Player player) {
		if(player.isFall()==false){
			if(player.y>110) {
				player.y-=9;
			}
			else {
				player.getTerminal().writeTerminal("High Jump");
				player.setFall(true);
			}
		}
		if(player.isFall()==true) {
			if(player.y<245) {
				player.y+=9;
			}
			else {
				player.setFall(false);
				player.getKeyboardInput().jump=false;
				if(player.y!=245) {
					player.y=245;
				}
			}
		}
	}
}
