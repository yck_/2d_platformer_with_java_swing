package Jump;
import GameObjects.Player;

public class LowJump implements JumpBehavior {

	@Override
	public void jump(Player player) {
		if(player.isFall()==false){
			if(player.y>150) {
				player.y-=7;
			}
			else {
				player.getTerminal().writeTerminal("Low Jump");
				player.setFall(true);
			}
		}
		if(player.isFall()==true) {
			if(player.y<245) {
				player.y+=7;
			}
			else {
				player.setFall(false);
				player.getKeyboardInput().jump=false;
				if(player.y!=245) {
					player.y=245;
				}
			}
		}
	}
}
