package Jump;
import GameObjects.Player;

public interface JumpBehavior {
	
	/**
	 * jump behavior for player.
	 * @param player
	 */
	public void jump(Player player);
	
}
