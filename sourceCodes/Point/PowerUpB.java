package Point;

public class PowerUpB extends Boost{
	
	public PowerUpB(Point point) {
		super(point);
	}
	
	public long calculatePoint() {
		return 5*(point.calculatePoint());
	}
	
}
