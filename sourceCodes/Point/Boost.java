package Point;
/**
 * Decorator pattern Boost class
 * @author yusuf
 *
 */
public abstract class Boost extends Point{
	Point point;
	
	public Boost(Point point) {
		this.point=point;
	}
	public abstract long calculatePoint();
	
}
