package Point;

public class PowerUpC extends Boost{
	
	public PowerUpC(Point point) {
		super(point);
	}
	
	public long calculatePoint() {
		return 10*(point.calculatePoint());
	}
	
}
