package Point;

public class PowerUpA extends Boost{

	public PowerUpA(Point point) {
		super(point);
	}
	
	public long calculatePoint() {
		return 2*(point.calculatePoint());
	}
	
}
