package Main;

import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;


/**
 *
 * Includes panel properties.
 *
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel {
	
	int width,height;

	/**
	 * 
	 * @param x x
	 * @param y y 
	 * @param width width
	 * @param height height
	 */
	public GamePanel(int x,int y,int width,int height) {
		this.width=width;
		this.height=height;
		this.setBounds(x, y, width, height);
		this.setBackground(new Color(0,205,255));
		this.setDoubleBuffered(true);
		this.setFocusable(true);
	}
	
	/**
	 * draw the game panel
	 * @param bufImg  buffered image that will drawn.
	 */
	public void draw(BufferedImage bufImg) {
		Graphics g2=(Graphics) this.getGraphics();
		g2.drawImage(bufImg,0,0,width,height,null);
		g2.dispose();
		
	}
}
