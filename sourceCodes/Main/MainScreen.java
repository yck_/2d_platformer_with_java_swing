package Main;
import java.awt.image.BufferedImage;
import Input.KeyboardInput;
import Input.MouseInput;
import Screens.GameOverScreen;
import Screens.GameScreen;
import Screens.MenuScreen;
import Screens.Screen;
import Screens.ScreenVisualiser;
import java.awt.Graphics2D;
import java.awt.Color;
 
/**
 * The main thread screen for the window. It has the main update and draw loop and
 * 	all the keyboard and mouse functionalities.
 *
 */
public class MainScreen implements Runnable {

	private Screen gameWindow;
	private BufferedImage bufImg;
	private boolean isRunning;
	private Graphics2D graphGame;
	private Thread thread;
	private KeyboardInput keyboardInput;
	private ScreenVisualiser playScreen;
	private boolean pause=false;
	private ScreenVisualiser gameScreen;
	private ScreenVisualiser menuScreen;
	private ScreenVisualiser gameOverScreen;
	private MouseInput mouseInput;
	
	/**
	 * 
	 * @param width width of main game
	 * @param height height of main game
	 * @param game screen window name
	 */
	public MainScreen(int width,int height,String game) {
		gameWindow=new Screen(width,height,game);
		keyboardInput=new KeyboardInput(this);	
		gameScreen=new GameScreen(this,keyboardInput,gameWindow);
		menuScreen=new MenuScreen(this);
		gameOverScreen=new GameOverScreen(this);
		mouseInput=new MouseInput(menuScreen,gameOverScreen,gameScreen);
		playScreen=menuScreen;
	}
	
	/**
	 * initialization
	 */
	public void init() {
		gameWindow.getFrame().addKeyListener(keyboardInput);
		gameWindow.getFrame().addMouseListener(mouseInput);
		gameWindow.getFrame().addMouseMotionListener(mouseInput);
		gameWindow.getGp().addMouseListener(mouseInput);
		gameWindow.getGp().addMouseMotionListener(mouseInput);
		isRunning=true;
		bufImg = new BufferedImage(640,360,BufferedImage.TYPE_INT_ARGB);
		graphGame= (Graphics2D)bufImg.getGraphics();
		playScreen.init();
	}
	
	@Override
	public void run() {
		init();
		
		int fps = 40;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now,timer=0;
		long lastTime = System.nanoTime();
		int ticks = 0;
		while(isRunning) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;
			
			if(delta >= 1){
				if(!pause) {
					update();
					draw();
				}
				ticks++;
				delta--;
			}
			if(timer >= 1000000000){
				gameWindow.setFPS(ticks);
				//gameWindow.writeTerminal("Ticks and Frames: " + ticks);
				//System.out.println("Ticks and Frames: " + ticks);
				timer = 0;
				ticks = 0;
			}
		}
	}
	
	/**
	 * update
	 */
	public void update() {
		playScreen.update();
	}
	
	/**
	 * rendering
	 */
	public void draw() {
		graphGame.clearRect(0, 0, 640, 480);
		graphGame.setBackground(new Color(0,205,255));

		playScreen.draw(graphGame);
		gameWindow.draw(bufImg);	
	}
	
	/**
	 * thread start
	 */
	public synchronized void start(){
		if(isRunning)
			return;
		isRunning = true;
		thread = new Thread(this);
		thread.start();
	}
	
	/**
	 * thread stop
	 */
	public synchronized void stop(){
		if(!isRunning)
			return;
		isRunning = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public MouseInput getMouseInput() {
		return mouseInput;
	}
	
	public void switchGame() {
		mouseInput.menuDisable();
		playScreen=gameScreen;
		gameWindow.getFrame().requestFocus();
	}
	
	public void resetGame(){
		mouseInput.gameOverDisable();
		((GameScreen)gameScreen).restartGame(keyboardInput,gameWindow);
		mouseInput.gameEnable();
		playScreen=gameScreen;
		gameWindow.setFPSvisible(true);
	}
	
	public void switchGameOver() {
		playScreen=gameOverScreen;
		mouseInput.gameOverEnable();
		gameWindow.setFPSvisible(false);
	}
	
	public Screen getGameWindow() {
		return gameWindow;
	}

	public boolean isPause() {
		return pause;
	}

	public void setPause(boolean pause) {
		this.pause = pause;
	}

	    
}
