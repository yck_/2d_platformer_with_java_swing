package Input;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import Screens.GameOverScreen;
import Screens.GameScreen;
import Screens.MenuScreen;
import Screens.ScreenVisualiser;

public class MouseInput implements MouseListener, MouseMotionListener{
	
	private boolean leftButton;
	private int x,y;
	private MenuScreen menuScreen;
	private GameOverScreen gameOverScreen;
	private boolean menuEnable=true;
	private boolean gameOverEnable=false;
	private boolean gameEnable=false;
	private GameScreen gameScreen;

	public MouseInput(ScreenVisualiser menuScreen,ScreenVisualiser gameOverScreen,ScreenVisualiser gameScreen){
		super();
		this.menuScreen=(MenuScreen)menuScreen;
		this.gameOverScreen=(GameOverScreen)gameOverScreen;
		this.gameScreen=(GameScreen)gameScreen;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	
	public boolean getLeftButton() {
		return leftButton;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		x = e.getX();
		y = e.getY();
		if(menuEnable) {
			menuScreen.mouseMoved(e);
		}
		else if(gameOverEnable) {
			gameOverScreen.mouseMoved(e);
		}
		else if(gameEnable) {
			gameScreen.mouseMoved(e);
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(menuEnable) {
			menuScreen.click();
		}
		else if(gameOverEnable) {
			gameOverScreen.click();
		}
		else if(gameEnable) {
			gameScreen.click();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton()==MouseEvent.BUTTON1) {
			leftButton=true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getButton()==MouseEvent.BUTTON3) {
			leftButton=false;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Disables the main menu events on mouse inputs.
	 */
	public void menuDisable() {
		menuEnable=false;
	}
	
	public void gameOverDisable() {
		gameOverEnable=false;
	}
	
	public void gameOverEnable() {
		gameOverEnable=true;
	}
	
	public void gameEnable() {
		gameEnable=true;
		menuEnable=false;
		gameOverEnable=false;
	}
}
