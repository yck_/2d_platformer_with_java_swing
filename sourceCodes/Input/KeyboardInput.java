package Input;
import java.awt.event.KeyEvent;

import Main.MainScreen;

import java.awt.event.KeyAdapter;

public class KeyboardInput extends KeyAdapter{

	public boolean jump,run,pause;
	
	/*
	 * Key adapter method for keyboard inputs 
	 * @param thread
	 */
	public KeyboardInput(MainScreen thread){
		super();
		jump=false;
		run=false;
		pause=false;
	}
	@Override
	public void keyPressed(KeyEvent e) {

			switch(e.getKeyCode()) {
			case KeyEvent.VK_D:
				if(run!=true) {
					run=true;
				}
				break;
			case KeyEvent.VK_SPACE:
				if(jump!=true) {
					jump=true;
				}
				break;
			}	

	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_D) {
			run=false;
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

}
