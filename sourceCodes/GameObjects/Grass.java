package GameObjects;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Grass extends GameObjects{
	
	private BufferedImage grassAsset;
	public static final int GRASS_WIDTH = 40;
	public static final int GRASS_HEIGHT = 80;
	
	/**
	 * 
	 * @param x x 
	 * @param y y
	 * @param width width
	 * @param height height
	 */
	public Grass(int x, int y, int width, int height) {
		super(x,y,GRASS_WIDTH,GRASS_HEIGHT);
		loadAssetImages();
	}	
	
	/**
	 * updates the grasses location with respect to updatecount
	 */
	public void move(int place,int updateCount) {
		x=(place*GRASS_WIDTH)-updateCount;
	}
	
	/**
	 * draw images
	 */
	public void draw(Graphics2D g) {
		g.drawImage(grassAsset,this.x, this.y,null);
	}
	
	
	public void update() {}
	
	/**
	 * loads the asset in initialization step.
	 */
    public void loadAssetImages() {
        try {
        	grassAsset = ImageIO.read(getClass().getResource("../img/grass.png"));
        } catch (IOException exc) {
            System.out.println("Error opening image file: " + exc.getMessage());
        }
    }
    
}
