package GameObjects;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;


public class BigSlime extends GameObjects{

	public static final int SLIME_WIDTH = 54;
	public static final int SLIME_HEIGHT = 45;
	public int sheetIndex=0;
	private BufferedImage bigSlime[];
	
	/**
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param width width of slime
	 * @param height height of slime
	 */
	public BigSlime(int x, int y, int width, int height) {
		super(x, y, SLIME_WIDTH, SLIME_HEIGHT);
		init();
	}

	/**
	 * move slime with given updatecount
	 */
	public void move(int place,int updateCount) {
		x=x-updateCount;
	}
	
	/**
	 * update
	 */
	public void update() {
		sheetIndex+=1;
		sheetIndex%=12;
	}
	
	/**
	 * initialization
	 */
	public void init() {
		try {
			Random rand = new Random();
			bigSlime=new BufferedImage[12];
			
			BufferedImage bigSlimeSheet;

			if(rand.nextInt(3)==0) {
				bigSlimeSheet=ImageIO.read(getClass().getResource("../img/big_slime_purple.png"));
			}
			else if(rand.nextInt(3)==1){
				bigSlimeSheet=ImageIO.read(getClass().getResource("../img/big_slime_green.png"));
			}
			else {
				bigSlimeSheet=ImageIO.read(getClass().getResource("../img/big_slime_orange.png"));
			}
			
			for(int i=0;i<4;i++) {
				bigSlime[3*i]=bigSlimeSheet.getSubimage(4,i*64,54,45);
				bigSlime[3*i+1]=bigSlimeSheet.getSubimage(69,i*64,54,45);
				bigSlime[3*i+2]=bigSlimeSheet.getSubimage(133,i*64,54,45);
			}
    		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * rendering
	 */
	public void draw(Graphics2D g) {
		g.drawImage(bigSlime[sheetIndex],x,y,null);
	}

}

