package GameObjects;
import java.awt.Graphics2D;

public abstract class GameObjects {
	
	public int x;
	public int y;
	public int width;
	public int height;
	
	/**
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param width width
	 * @param height height
	 */
	public GameObjects(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Move object with given updateCount
	 * @param place place parameter for drawing grass. Can take any value with different objects.
	 * @param updateCount movement speed.
	 */
	public abstract void move(int place,int updateCount);

	/**
	 * render
	 * @param g graphics object
	 */
	public abstract void draw(Graphics2D g);

	/**
	 * update
	 */
	public abstract void update();
	
	
	
}
