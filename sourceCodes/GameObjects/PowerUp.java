package GameObjects;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;

public class PowerUp extends GameObjects{
	
	public static final int PowerUp_WIDTH = 40;
	public static final int PowerUp_HEIGHT = 40;
	public String powerUpType="";
	private BufferedImage powerUp;
	
	public PowerUp(int x, int y, int width, int height) {
		super(x, y, PowerUp_WIDTH,PowerUp_WIDTH);
		init();
	}

	public void move(int place,int updateCount) {
		x=x-updateCount;
	}

	@Override
	public void update() { }
	
	/**
	 * initializes the powerUp asset
	 */
	public void init() {
		try {
		Random rand = new Random();
		//Obstacle=new BufferedImage();
		
		BufferedImage obstacleSheet;

			if(rand.nextInt(3)==0) {
				obstacleSheet=ImageIO.read(getClass().getResource("../img/powerUpA.png"));
				powerUp=obstacleSheet.getSubimage(0,0,40,40);
				powerUpType="A";
			}
			else if(rand.nextInt(3)==1){
				obstacleSheet=ImageIO.read(getClass().getResource("../img/powerUpB.png"));
				powerUp=obstacleSheet.getSubimage(0,0,40,40);
				powerUpType="B";
			}
			else {
				obstacleSheet=ImageIO.read(getClass().getResource("../img/powerUpC.png"));
				powerUp=obstacleSheet.getSubimage(0,0,40,40);
				powerUpType="C";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void draw(Graphics2D g) {
		g.drawImage(powerUp,x,y,null);
	}
	
	public String getPowerUpType() {
		return powerUpType;
	}

}