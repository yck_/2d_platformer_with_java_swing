package GameObjects;


import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;


public class Obstacle extends GameObjects{
	
	public int OBSTACLE_WIDTH = 32;
	public int OBSTACLE_HEIGHT = 32;
	public int sheetIndex=0;
	private BufferedImage obstacle;
	
	
	public Obstacle(int x, int y, int width, int height) {
		super(x, y, 0, 0);
		init();
	}

	/**
	 * updates the obstacle place with given updateCount speed
	 * @param place
	 * @param updateCount speed
	 */
	public void move(int place,int updateCount) {
		x=x-updateCount;
	}
	
	/**
	 * updates the obstacle animation
	 */
	public void update() {
		sheetIndex+=1;
		sheetIndex%=4;
	}
	
	/**
	 * initialization method
	 */
	public void init() {
		try {
		Random rand = new Random();
		//Obstacle=new BufferedImage();
		
		BufferedImage obstacleSheet;

			if(rand.nextInt(3)==0) {
				obstacleSheet=ImageIO.read(getClass().getResource("../img/rockhead.png"));
				obstacle=obstacleSheet.getSubimage(0,0,32,32);
				this.width=32;
				this.height=32;
				this.y=252;
			}
			else if(rand.nextInt(3)==1){
				obstacleSheet=ImageIO.read(getClass().getResource("../img/spikes.png"));
				obstacle=obstacleSheet.getSubimage(0,0,50,52);
				this.width=40;
				this.height=40;
				this.y=228;
			}
			else {
				obstacleSheet=ImageIO.read(getClass().getResource("../img/spikehead.png"));
				obstacle=obstacleSheet.getSubimage(0,0,50,52);
				this.width=30;
				this.height=40;
				this.y=235;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * draws the obstacle to its x,t coordinate
	 */
	public void draw(Graphics2D g) {
		g.drawImage(obstacle,x,y,null);
	}

}