package GameObjects;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class JumpBoost extends GameObjects{
	public static final int BOOST_WIDTH =10;
	public static final int BOOST_HEIGHT = 10;
	public int sheetIndex=0;
	private BufferedImage jumpAsset[];
	
	/**
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param width width
	 * @param height height
	 */
	public JumpBoost(int x, int y, int width, int height) {
		super(x, y, BOOST_WIDTH, BOOST_HEIGHT);
		init();
	}

	/**
	 * update location
	 * @param place 
	 * @param updateCount for movement speed of jumpboost asset.
	 */
	public void move(int place,int updateCount) {
		x=x-updateCount;
	}
	
	/**
	 * daws the image asset.
	 */
	public void draw(Graphics2D g) {
		g.drawImage(jumpAsset[sheetIndex],x,y,null);
	}

	@Override
	public void update() {
		sheetIndex+=1;
		sheetIndex%=10;
	}
	
	/**
	 * initialization step
	 */
	public void init(){
		try {
		jumpAsset=new BufferedImage[10];
		
		BufferedImage jumpPowerUpSheet;
		jumpPowerUpSheet=ImageIO.read(getClass().getResource("../img/highJump.png"));

		//Crop sheets
    	for(int i=0;i<10;i++) {
    		jumpAsset[i]=jumpPowerUpSheet.getSubimage(i*18,0,18,18);
    	}
    	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}