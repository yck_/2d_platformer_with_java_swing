package GameObjects;
import java.awt.Graphics2D;
import javax.imageio.ImageIO;

import Input.KeyboardInput;
import Jump.HighJump;
import Jump.JumpBehavior;
import Jump.LowJump;
import Main.MainScreen;
import Point.JumpPoint;
import Point.Point;
import Point.PowerUpA;
import Point.PowerUpB;
import Point.PowerUpC;
import Screens.Screen;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Player class that has all the player functionalities and variables such as point.
 * It includes drawing character, moving character and etc.
 *
 */
public class Player extends GameObjects {
	
	//assets
	private BufferedImage[] playerRunAsset;
	private BufferedImage[] playerIdleAsset;
	private BufferedImage playerJumpAsset;
	private BufferedImage playerFallAsset;
	private BufferedImage currentAsset;
	private BufferedImage healthAsset;
	
	private int runIndex=0;
	private int idleIndex=0;
	protected boolean fall;

	private long now=0;
	private long lastTime=0;
	private long timer=0;
	public int health=3;
	private int old_health=3;
	
	protected KeyboardInput keyboardInput;
	private JumpBehavior jumpBehavior;
	private Point playerPoint=null;
	private int totalPoint=0;
	
	private Screen terminal;
	private MainScreen mainScreen;
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param keyboardInput KeyAdapter object for keyboard inputs
	 * @param terminal Screen object for updating game terminal
	 */ 
	public Player(int x, int y, int width, int height,KeyboardInput keyboardInput,Screen terminal,MainScreen mainScreen) {
		super(50, 246, 15, 20);
		this.keyboardInput=keyboardInput;
		fall=false;
		playerRunAsset=new BufferedImage[8];
		playerIdleAsset=new BufferedImage[12];
		loadAssetImages();
		jumpBehavior=new LowJump();
		playerPoint=new JumpPoint();
		this.terminal=terminal;
		this.mainScreen=mainScreen;
	}
	
	public JumpBehavior getJumpBehavior() {
		return jumpBehavior;
	}
	
	/**
	 * Does character falling
	 * @return
	 */
	public boolean isFall() {
		return fall;
	}

	public void setFall(boolean fall) {
		this.fall = fall;
	}

	public Screen getTerminal() {
		return terminal;
	}
		
	/**
	 * update the player movement(jump,run animations) and place.
	 */
	public void update() {
		
		if(keyboardInput.jump==true){
			jumpBehavior.jump(this);
		}
		
		if(fall==true) {
			currentAsset=playerFallAsset;
		}
		else if(keyboardInput.jump==true) {
			currentAsset=playerJumpAsset;
		}
		else if(keyboardInput.run==true) {
			updateRunIndex();
		}
		else if(keyboardInput.run==false && keyboardInput.jump==false) {
			updateIdleIndex();
		}
	}
		
	public void move(int place,int updateCount) {}
	
	/**
	 * draws the player and healtbar.
	 */
	public void draw(Graphics2D g) {
		g.drawImage(currentAsset, x,y,null);
		
		for(int i=0;i<health;i++) {
			g.drawImage(healthAsset,(i*30)+10,10,null);
		}
		
	}

	/**
	 * loads all the necessary assets in initialization step.
	 */
    public void loadAssetImages() {
        try {
        	healthAsset = ImageIO.read(getClass().getResource("../img/health.png"));
        	BufferedImage spriteSheetRun = ImageIO.read(getClass().getResource("../img/run.png"));
        	BufferedImage spriteSheetIdle = ImageIO.read(getClass().getResource("../img/idle2.png"));
        	playerJumpAsset = ImageIO.read(getClass().getResource("../img/jump.png"));
        	playerFallAsset = ImageIO.read(getClass().getResource("../img/landing.png"));
        	int x=0;
        	for(int j=0;j<2;j++) {
            	for(int i=0;i<4;i++) {
            		playerRunAsset[x]=spriteSheetRun.getSubimage(i*21,j*33,21,33);
            			x+=1;
            	}
        	}
        	x=0;
        	for(int j=0;j<3;j++) {
            	for(int i=0;i<5;i++) {
            		playerIdleAsset[x]=spriteSheetIdle.getSubimage(i*19,j*34,19,34);
            		if(x==11) {
            			break;
            		}
            		x+=1;
            	}
        		if(x==11) {
        			break;
        		}
        	}
        } catch (IOException exc) {
            System.out.println("Error opening image file: " + exc.getMessage());
        }
    }
    
    /**
     * updates the run animation
     */
    public void updateRunIndex() {
    	now = System.nanoTime();
    	timer += now - lastTime;

		if( timer>100000000) {
	    	runIndex+=1;	
	    	runIndex%=8;
			currentAsset=playerRunAsset[runIndex];
		   	timer=0;
	    	lastTime=0;
	    	now=0;
		}

    	lastTime = System.nanoTime();
    }
    
    /**
     * updates the idle animation
     */
    public void updateIdleIndex() {
    	now = System.nanoTime();
    	timer += now - lastTime;
		if( timer>150000000) {
	    	idleIndex+=1;	
	    	idleIndex%=8;
	    	currentAsset=playerIdleAsset[idleIndex];
	    	timer=0;
	    	lastTime=0;
	    	now=0;
		}
    	lastTime = System.nanoTime();
    }
    
    /**
     * updates the jump behavior using strategy pattern
     */
    public void switchJump() {
    	if(this.jumpBehavior instanceof HighJump) {
    		this.jumpBehavior=new LowJump();
    		terminal.writeTerminal("Jump Mode Changed: Low Jump");
    	}
    	else {
    		this.jumpBehavior=new HighJump();
    		terminal.writeTerminal("Jump Mode Changed: High Jump");
    	}
    }
    
    /**
     * decreases current player health
     */
    public void decreaseHealt(){
    	health--;
    	if(health==0) {
    		mainScreen.switchGameOver();
    	}
    }
    
    /**
     * calculates earned point with bonuses using decorator pattern and adds it to the totalPoint
     */
    public void earnPoint(){
    	if(health==old_health) {
        	totalPoint+=playerPoint.calculatePoint();
        	terminal.writeTerminal("Total Point:"+totalPoint);
    	}
    	else {
    		old_health=health;
    	}

    }
    
    /**
     * when player earns new powerUp it adds it to the current powerUps using decorator pattern.
     * @param powerUpType
     */
    public void earnPowerUpPoint(String powerUpType){
    	if(powerUpType.equals("A")) {
    		PowerUpA puA=new PowerUpA(playerPoint);
    		playerPoint=puA;
    		terminal.writeTerminal("PowerUp(x2) Boost: x"+puA.calculatePoint());
    	}
    	else if(powerUpType.equals("B")){
    		PowerUpB puB=new PowerUpB(playerPoint);
    		playerPoint=puB;
    		terminal.writeTerminal("PowerUp(x5) Boost: x"+puB.calculatePoint());
    	}
    	else {
    		PowerUpC puC=new PowerUpC(playerPoint);
    		playerPoint=puC;
    		terminal.writeTerminal("PowerUp(x10) Boost: x"+puC.calculatePoint());
    	}
    	
    }
    
    /**
     * Getter for keyboardListener
     * @return keyboardInput object
     */
	public KeyboardInput getKeyboardInput() {
		return keyboardInput;
	}

}



	
