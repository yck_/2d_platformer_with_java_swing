package Buttons;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

/**
 * Abstract button class that has 
 * 	x,y coordinate and width,height properties.
 *
 */
public abstract class Button {

	Rectangle button;
	public boolean hover=false;
	protected int width,height;
	protected int x,y;
	
	public Button(int x,int y,boolean hover, int width, int height) {
		super();
		this.button = new Rectangle(x,y,width,height);
		this.x=x;
		this.y=y;
		this.hover = hover;
		this.width = width;
		this.height = height;
	}
	
	public abstract void update();
	public abstract void draw(Graphics2D g);
	public abstract void click();
	
	/**
	 * does mouse over the button
	 * @param e mouse event for obtaining mouse coordinates
	 */
	public void overMouse(MouseEvent e) {

		if(button.contains(e.getX(),e.getY())) {
			hover=true;
		}
		else {
			hover=false;
		}
	}
	/**
	 * If user clicked the mouse or not.
	 */
	public void isClickMouse() {
		if(hover) {
			click();
		}
	}
	
	
	

	
	
	
}
