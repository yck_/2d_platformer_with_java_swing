package Buttons;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import Main.MainScreen;

public class PlayButton extends Button{
	
	private BufferedImage[] buttonAssets;
	MainScreen mainScreen;
	
 	public PlayButton(int x, int y, boolean hover, int width, int height,MainScreen mainScreen) {
		super(x, y, hover, width, height);
		buttonAssets=new BufferedImage[2];
		initAssets();
		this.mainScreen=mainScreen;
	}

	@Override
	public void update() {
	}

	@Override
	public void draw(Graphics2D g) {	
		if(hover) {
			g.drawImage(buttonAssets[0],x,y,null);
		}
		else {
			g.drawImage(buttonAssets[1],x,y,null);
		}
	}

	@Override
	public void click() {
		if(mainScreen.isPause()){
			mainScreen.setPause(false);
		}
	}
	
	public void initAssets(){
		try {
			BufferedImage buttonSheet1;
			BufferedImage buttonSheet2;
			buttonSheet1=ImageIO.read(getClass().getResource("../img/play_h.png"));
			buttonSheet2=ImageIO.read(getClass().getResource("../img/play.png"));
			buttonAssets[0]=buttonSheet1.getSubimage(0,0,40,40);
			buttonAssets[1]=buttonSheet2.getSubimage(0,0,40,40);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void mouseMoved(MouseEvent e) {
		overMouse(e);
	}
}
