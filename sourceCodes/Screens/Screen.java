package Screens;
import java.awt.Color;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.awt.Label;
import java.util.ArrayDeque;
import java.util.Deque;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import Main.GamePanel;


public class Screen {
	
	private JFrame frame;
	private GamePanel gp;
	private int width;
	private int height;
	private String name;
	private JTextArea textField;
	private Label fps;

	private Deque<String> terminal = new ArrayDeque<>();
	
	/**
	 * 
	 * @param width
	 * @param height
	 * @param name name of the screen
	 */
	public Screen(int width,int height,String name) {
		this.width=width;
		this.height=height;
		this.name=name;
		this.initFrame();
		this.initTextField();
		this.initPanel();
		initTextFieldFPS();
	}

	public JFrame getFrame() {
		return frame;
	}

	public GamePanel getGp() {
		return gp;
	}
	
	/**
	 * draws the given bufferedimage to the screen 
	 * @param bufImg bufferedimage that wanted to drawn
	 */
	public void draw(BufferedImage bufImg) {
		gp.draw(bufImg);
	}


	/**
	 * initializes the frame
	 */
	public void initFrame() {
		frame = new JFrame(name);
		frame.setSize(width,height);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	/**
	 * initializes the panel
	 */
	public void initPanel() {
		gp=new GamePanel(0,0,640,360);
		frame.add(gp,BorderLayout.CENTER);	
		frame.setVisible(true);
	}
	
	/**
	 * initializes the terminal text field
	 */
	public void initTextField() {
		for(int i=0;i<5;i++) {
			terminal.add("");
		}

        textField=new JTextArea();
        textField.setForeground(Color.white);
        textField.setBackground(Color.black);
        textField.setVisible(true);
        textField.setEditable(false);
        textField.setLineWrap(true);
        textField.setWrapStyleWord(true);
        textField.setBounds(0,360,640,120);
        textField.setFont(new Font("Book Antiqua",Font.BOLD|Font.ITALIC,13));
        frame.add(textField);
	}
	
	/**
	 * writes given string to game terminal
	 * @param s string
	 */
	public void writeTerminal(String s) {
		terminal.removeLast();
		terminal.addFirst("->"+s);
		updateTerminal();
	}
	
	/**
	 * updates the terminal. this method using inside the writeTerminal method. 
	 */
	public void updateTerminal() {
		StringBuilder stringBuilder = new StringBuilder();
		
        for (String item: terminal) {
        	stringBuilder.append(item+'\n');
        }
		textField.setText(stringBuilder.toString());
	}
	
	public void initTextFieldFPS() {
		fps = new Label("FPS:40");
		fps.setForeground(Color.black);
		fps.setVisible(true);
		fps.setBounds(0,0,30,30);
		fps.setFont(new Font("Book Antiqua", Font.BOLD, 10));

		gp.add(fps);
	}

	public void setFPS(int fpsValue) {
		fps.setText("FPS:"+String.valueOf(fpsValue));
	}
	
	public void setFPSvisible(Boolean visible) {
		fps.setVisible(visible);
	}
}