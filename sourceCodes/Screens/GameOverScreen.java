package Screens;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import Buttons.ExitButton;
import Buttons.RestartButton;
import Main.MainScreen;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

public class GameOverScreen extends ScreenVisualiser{
	
	private RestartButton restartButton;
	private ExitButton exitButton;
	private BufferedImage gameOverAsset;

	public GameOverScreen(MainScreen ms) {
		super();
		restartButton=new RestartButton(110, 240, false, 100, 100,ms);
		exitButton=new ExitButton(230, 240, false, 300, 102,ms);
		initAssets(); 
	}

	@Override
	public void draw(Graphics2D g) {
		g.drawImage(gameOverAsset,0,0,null);
		restartButton.draw(g);
		exitButton.draw(g);
	}

	@Override
	public void update() {
		restartButton.update();
		exitButton.update();
	}

	@Override
	public void init() {
		
	}
	
	public void click() {
		restartButton.isClickMouse();
		exitButton.isClickMouse();
	}
	
	public void mouseMoved(MouseEvent e) {
		restartButton.mouseMoved(e);
		exitButton.mouseMoved(e);
	}
	
	public RestartButton getRestartButton() {
		return restartButton;
	}
	
	public ExitButton getExitButton() {
		return exitButton;
	}
	
	
	public void initAssets(){
		try {
			gameOverAsset=ImageIO.read(getClass().getResource("../img/gameOver.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	



}
