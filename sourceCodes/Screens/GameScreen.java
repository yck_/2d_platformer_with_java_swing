package Screens;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import Buttons.PauseButton;
import Buttons.PlayButton;
import Buttons.QuitButton;
import GameMap.GameWord;
import Input.KeyboardInput;
import Main.MainScreen;


public class GameScreen extends ScreenVisualiser{

    private GameWord gameWord;
    private PlayButton playButton;
    private PauseButton pauseButton;
    private QuitButton quitButton;
    private MainScreen mainScreen;
    
	public GameScreen(MainScreen mainScreen,KeyboardInput keyboardInput,Screen terminal) {
        this.mainScreen=mainScreen;
        gameWord=new GameWord(mainScreen,keyboardInput,terminal);
        init();
    }

	public void init(){
        gameWord.init();
        playButton=new PlayButton(470, 5, false, 40, 40,mainScreen);
        pauseButton=new PauseButton(525, 5, false, 40, 40,mainScreen);
        quitButton=new QuitButton(580, 5, false, 40, 40,mainScreen);
    }
	
	public void restartGame(KeyboardInput keyboardInput,Screen terminal) {
		init();
        gameWord=new GameWord(mainScreen,keyboardInput,terminal);
	}
	

    @Override
    public void draw(Graphics2D g) {
    	playButton.draw(g);
    	pauseButton.draw(g);
    	quitButton.draw(g);
        gameWord.draw(g);
    }

    @Override
    public void update() {
        gameWord.update();
    }
    
    public PlayButton getPlayButton() {
 		return playButton;
 	}
    
    public PauseButton getPauseButton() {
 		return pauseButton;
 	}
    
    public QuitButton getQuitButton() {
 		return quitButton;
 	}
    
	public void click() {
		playButton.isClickMouse();
		pauseButton.isClickMouse();
		quitButton.isClickMouse();
	}
    
	public void mouseMoved(MouseEvent e) {
		playButton.mouseMoved(e);
		pauseButton.mouseMoved(e);
		quitButton.mouseMoved(e);
	}

}