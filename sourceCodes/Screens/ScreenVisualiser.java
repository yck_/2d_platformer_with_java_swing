package Screens;
import java.awt.Graphics2D;

public abstract class ScreenVisualiser {
	
	public ScreenVisualiser() {
	}
	
	public abstract void draw(Graphics2D g);
	
	public abstract void update();
	
	public abstract void init();
}
