package Screens;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import Buttons.ExitButton;
import Buttons.StartButton;
import Main.MainScreen;

public class MenuScreen extends ScreenVisualiser{
	
	private StartButton startButton;
	private ExitButton exitButton;
	
	public MenuScreen(MainScreen ms) {
		super();
		startButton=new StartButton(140, 50, false, 300, 102,ms);
		exitButton=new ExitButton(140, 200, false, 300, 102,ms);
	}

	@Override
	public void draw(Graphics2D g) {
		startButton.draw(g);
		exitButton.draw(g);
	}

	@Override
	public void update() {
		startButton.update();
		exitButton.update();
	}

	@Override
	public void init() {
		
	}
	
	public void click() {
		startButton.isClickMouse();
		exitButton.isClickMouse();
	}
	
	public void mouseMoved(MouseEvent e) {
		startButton.mouseMoved(e);
		exitButton.mouseMoved(e);
	}
	
	public StartButton getStartButton() {
		return startButton;
	}
	
	public ExitButton getExitButton() {
		return exitButton;
	}



}
