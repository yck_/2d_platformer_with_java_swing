package GameMap;

import java.awt.Graphics2D;

import GameObjects.Player;


public class Enviroment {
	protected Player player;
	private Floor floor;
	
	/**
	 * Enviroment class
	 * @param player player object
	 */
	public Enviroment(Player player) {
		floor=new Floor();
		this.player=player;
	}
	
	/**
	 * update
	 */
	public void update() {
		floor.update(player.getKeyboardInput().run);
	}
	
	/**
	 * draw the frame
	 * @param g graphics object
	 */
	public void draw(Graphics2D g) {
		floor.draw(g);
	}
}
