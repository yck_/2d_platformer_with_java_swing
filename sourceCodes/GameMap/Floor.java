package GameMap;


import java.awt.Graphics2D;

import GameObjects.Grass;

/**
 * Generates floor objects
 * @author Yusuf Can Kan
 *
 */
public class Floor{

	private Grass[] grass;
	private int updateCount=0;

	/**
	 * Generates floor objects
	 */
	Floor(){
		grass=new Grass[20];
		
		for(int i=-1;i<19;i++) {
	        grass[i+1]=new Grass(i*40, 280, 40, 40);
		}
	}
	
	/**
	 * update
	 * @param run boolean value which represents if character is running or not.
	 */
	public void update(boolean run) {
		if(run==true) {
			for(int i=-1;i<19;i++) {
				grass[i+1].move(i,updateCount);
			}
			updateCount+=4;
			updateCount%=40;
		}
	}

	/**
	 * draw the floor
	 * @param g graphics object
	 */
	public void draw(Graphics2D g) {
		for(int i=0;i<20;i++) {
			grass[i].draw(g);
		}
	}
    
}


	

