package GameMap;
import java.awt.Graphics2D;

import GameObjects.BigSlime;
import GameObjects.GameObjects;
import GameObjects.Obstacle;
import GameObjects.Player;
import Input.KeyboardInput;
import Main.MainScreen;
import Screens.Screen;

/**
 * Creates the all word for the user.
 *
 */
public class GameWord {
	
	protected Player player;
	private Enviroment enviroment;
	private MapGenerator mapGenerator;
	private Collision collision;
	private boolean pointFlag[];
	
	/**
	 * Generates whole game map.
	 * @param keyboardInput KeyListener object.
	 * @param gameTerminal Screen object for writing values to terminal.
	 */
	public GameWord(MainScreen mainScreen,KeyboardInput keyboardInput,Screen gameTerminal){
		player=new Player(10,20,50,50,keyboardInput,gameTerminal,mainScreen);
		enviroment=new Enviroment(player);
		mapGenerator=new MapGenerator(keyboardInput);
		collision=new Collision(player,mapGenerator.getObstacles());
		pointFlag=new boolean[mapGenerator.getObstacleArrayCount()];
	}
	
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * initialization
	 */
	public void init() {
		mapGenerator.init();
		for(int i=0;i<mapGenerator.getObstacleArrayCount();i++) {
			pointFlag[i]=false;
		}
	}
	 /**
	  * update
	  * enviroment,player,map generator and collision objects is updating,
	  * Checking the point status for player.
	  */
	public void update() {
		enviroment.update();
		player.update();
		mapGenerator.update();
		collision.update();
		
		for(int i=0;i<mapGenerator.getObstacleArrayCount();i++) {
			if(mapGenerator.getIsRemoved()[i] || collision.getIsRemoved()[i]) {
				pointFlag[i]=false;
				collision.setIsDamaged(false);
			}
		}
		mapGenerator.resetIsRemoved();
		collision.resetIsRemoved();
		isPointEarned();
		mapGenerator.addCurrentObstacleCount(collision.getDisappearedElements());
		collision.setDisappearedElements(0);
	}
	
	/**
	 * player enviroment and map drawn 
	 * @param g graphics object that will be drawn
	 */
	public void draw(Graphics2D g){
		player.draw(g);
		enviroment.draw(g);
		mapGenerator.draw(g);
	}
	
	/**
	 * check if player earned point or not.
	 */
	public void isPointEarned() {
		GameObjects[] obstacles=mapGenerator.getObstacles();
		for(int i=0;i<mapGenerator.getObstacleArrayCount();i++) {
			if(obstacles[i]!=null && obstacles[i].x<player.x && pointFlag[i]!=true 
					&& (obstacles[i] instanceof Obstacle || obstacles[i] instanceof BigSlime)) {
					player.earnPoint();
					pointFlag[i]=true;
			}
		}
	}

}
