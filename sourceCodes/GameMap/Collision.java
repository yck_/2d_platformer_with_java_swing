package GameMap;
import GameObjects.BigSlime;
import GameObjects.GameObjects;
import GameObjects.JumpBoost;
import GameObjects.Obstacle;
import GameObjects.Player;
import GameObjects.PowerUp;

/**
 * Collision class for managing all game objects collision situations with player,
 *
 */
public class Collision {
	
	private Player player;
	private GameObjects[] obstacles;
	private int disappearedElements=0;
	private boolean isRemoved[];
	private boolean isDamaged=false;
	
	/**
	 * Responsible class for collision system
	 * @param player player object for obtaining players coordinate
	 * @param obstacles current obstacles in the map.
	 */
	Collision(Player player,GameObjects[] obstacles){
		this.player=player;
		this.obstacles=obstacles;
		isRemoved=new boolean[5];
		resetIsRemoved();
	}
	
	/**
	 * getter
	 * @return removed objects in current update operation
	 */
	public boolean[] getIsRemoved() {
		return isRemoved;
	}
	
	/**
	 * update. It checks collision states.
	 */
	void update(){
		for(int i=0;i<obstacles.length;i++) {
			if(obstacles[i]!=null) {
				if( (((player.x>obstacles[i].x) && (player.x<(obstacles[i].x+obstacles[i].width))) ||
						(((player.x+player.width)>obstacles[i].x) && 
								((player.x+player.width)<(obstacles[i].x+obstacles[i].width))))
					&& 
					(((player.y>(obstacles[i].y)) && player.y<(obstacles[i].y+obstacles[i].height)) ||
						((player.y+player.height)>obstacles[i].y && 
								(player.y+player.height)<(obstacles[i].y+obstacles[i].height)))){
					
					collisionEvent(obstacles[i],i);
					if(!(obstacles[i] instanceof Obstacle || obstacles[i] instanceof BigSlime)) {
						obstacles[i]=null;
						isRemoved[i]=true;
						disappearedElements--;
					}
				}
			}
		}
	}
	
	/**
	 * Action for result of collided obstacle
	 * @param obstacle colided obstacle
	 * @param i place in the obstacles array.
	 */
	void collisionEvent(GameObjects obstacle,int i) {
		if(obstacle instanceof JumpBoost) {
			player.switchJump();
		}
		else if(obstacle instanceof Obstacle || obstacle instanceof BigSlime) {
			if(!isDamaged) {
				player.decreaseHealt();
				isDamaged=true;
			}		
		}
		else if(obstacle instanceof PowerUp){
			PowerUp power=((PowerUp)obstacle);
			player.earnPowerUpPoint(power.getPowerUpType());
		}
	}

	/**
	 * gets the removed elements from scene
	 * @return
	 */
	public int getDisappearedElements() {
		return disappearedElements;
	}
	
	/**
	 * Sets the removed elements from the scene
	 * @param collidedElements
	 */
	public void setDisappearedElements(int collidedElements) {
		this.disappearedElements = collidedElements;
	}
	
	/**
	 * resets the isRemoved array. Sets all values to false.
	 */
	public void resetIsRemoved() {
		for(int i=0;i<5;i++) {
			isRemoved[i]=false;
		}
	}
	
	/**
	 * if current obstacle is damamaged to player, this method helps to set isDamaged parameter for the obstacle.
	 * @param isDamaged true or false.
	 */
	public void setIsDamaged(boolean isDamaged) {
		this.isDamaged = isDamaged;
	}

	
	
}
