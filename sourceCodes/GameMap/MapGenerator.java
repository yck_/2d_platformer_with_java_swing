package GameMap;
import java.awt.Graphics2D;
import java.util.Random;

import GameObjects.BigSlime;
import GameObjects.GameObjects;
import GameObjects.JumpBoost;
import GameObjects.Obstacle;
import GameObjects.PowerUp;
import Input.KeyboardInput;


/**
 * 
 * Generates random obstacle for the player.
 *
 */
public class MapGenerator {
	
	private GameObjects[] obstacles;
	private Random rand;
	private KeyboardInput keyboardInput;
	private int obstacleArrayCount=5;
	private int currentObstacleCount=0;
	private boolean isRemoved[];
	
	/**
	 * Map generator class constructor. Constructs random map.
	 * @param keyboardInput keyboard object for key inputs.
	 */
	MapGenerator(KeyboardInput keyboardInput){
		rand = new Random();
		obstacles=new GameObjects[5];
		this.keyboardInput=keyboardInput;
		isRemoved=new boolean[5];
		resetIsRemoved();
	}

	public boolean[] getIsRemoved() {
		return isRemoved;
	}

		
	public GameObjects[] getObstacles() {
		return obstacles;
	}
	
	public int getObstacleArrayCount() {
		return obstacleArrayCount;
	}
	
	/**
	 * initialization
	 * At the beginning it creates random obstacles to the stage.
	 */
	public void init() {
		for(int i=0;i<obstacleArrayCount;i++) {
			switch( (rand.nextInt(8))) {
			case 0:
				obstacles[i]=new BigSlime(600-(i*100),236,0,0);
				currentObstacleCount++;
				break;
			case 1:
				obstacles[i]=new JumpBoost(600-(i*100),240,0,0);
				currentObstacleCount++;
				break;
			case 3:
				obstacles[i]=new Obstacle(600-(i*100),240,0,0);
				currentObstacleCount++;
				break;
			case 4:
				obstacles[i]=new PowerUp(600-(i*100),220,0,0);
				currentObstacleCount++;
				break;
			case 5:
			case 6:
			case 7:
				obstacles[i]=null;
				break;
			}
		}
	}
	
	/**
	 * updates the obstacles coordinates and if there are few obstacles left in the stage 
	 * it produces more.
	 * update
	 */
	public void update() {
		if(keyboardInput.run==true) {
			for(int i=0;i<obstacleArrayCount;i++) {
				if(obstacles[i]!=null) {
					obstacles[i].move(0,4);
				}
			}
		}
		
		for(int i=0;i<obstacleArrayCount;i++) {
			if(obstacles[i]!=null) {
				obstacles[i].update();
			}
		}
		
		for(int i=0;i<obstacleArrayCount;i++) {
			if(obstacles[i]!=null && obstacles[i].x<-10) {
				obstacles[i]=null;
				isRemoved[i]=true;
				currentObstacleCount--;
				obstacles[i]=generateWordObsticle();
				setProperPlace(i);
			}
		}
		if(currentObstacleCount==0 || currentObstacleCount==1){
			if(obstacles[0]==null) {
				obstacles[0]=generateWordObsticle();
				setProperPlace(0);
			}
			if(obstacles[1]==null) {
				obstacles[1]=generateWordObsticle();
				if(obstacles[1]!=null) {
					obstacles[1].x=700;
					setProperPlace(1);
				}
			}
			if(obstacles[2]==null) {
				obstacles[2]=generateWordObsticle();
				if(obstacles[2]!=null) {
					obstacles[2].x=740;
					setProperPlace(2);
				}
			}
			if(obstacles[3]==null) {
				obstacles[3]=generateWordObsticle();
				if(obstacles[3]!=null) {
					obstacles[3].x=780;
					setProperPlace(3);
				}
			}
			if(obstacles[4]==null) {
				obstacles[4]=generateWordObsticle();
				if(obstacles[4]!=null) {
					obstacles[4].x=820;
					setProperPlace(4);
				}
			}
		}
	}

	/**
	 * draws each obstacle.
	 * @param g graphics object.
	 */
	public void draw(Graphics2D g) {
		for(int i=0;i<obstacleArrayCount;i++) {
			if(obstacles[i]!=null) {
				obstacles[i].draw(g);
			}
		}
	}
	
	/**
	 * generates random obstacle.
	 * @return generated obstacle
	 */
	public GameObjects generateWordObsticle() {
		switch(rand.nextInt(7)) {
			case 0:
			case 1:
				currentObstacleCount++;
				return (new BigSlime(640,236,10,10));
			case 2:
				return null;
			case 3:
			case 4:
				currentObstacleCount++;
				return (new Obstacle(640,240,10,10));
			case 5:
				currentObstacleCount++;
				return (new PowerUp(640,200,10,10));
				
			case 6:
				currentObstacleCount++;
				return (new JumpBoost(640,240,10,10));
		}
		return null;
	}
	
	/**
	 * checks if produced obstacle is colliding with other obstacles.
	 * @param x obstacle index from obstacles array.
	 * @return
	 */
	public boolean checkPlace(int x) {
		for(int i=0;i<obstacleArrayCount;i++) {
			if(x==i || obstacles[i]==null) continue;
			if(obstacles[x].x> obstacles[i].x-120 && obstacles[x].x < obstacles[i].x+120 ) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * sets a new place to given obstacle
	 * @param i obstacle index from obstacles array.
	 */
	public void setProperPlace(int i) {
		if(obstacles[i]!=null) {
			while(checkPlace(i)) {
				obstacles[i].x+=100;
			}
		}
	}
	
	/**
	 * updates the obstacle count
	 * @param count new value
	 */
	public void addCurrentObstacleCount(int count) {
		currentObstacleCount+=count;
	}
	
	/**
	 * sets false to all isRemoved array
	 */
	public void resetIsRemoved() {
		for(int i=0;i<5;i++) {
			isRemoved[i]=false;
		}
	}
}
